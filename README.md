# README #

Developed by Michael Egbo

See demo on http://site364.digitalskillsacademy.me 

CarShare is a digital product that allows users to share rides. Congestion in our cities has given rise to the need for more sustainable means of transportation. The average working class needs to be able to spend less time commuting, less time spent commuting translates to higher productivity, better quality of life, and life expectancy. Also, car sharing means less cars on the road, meaning less emissions for cleaner air and reduce rapid results of climate change.
Carshare provides an economical and environmentally friendly means of commute. It allows users to book a ride, and gives users the option to ride alongside other riders heading towards similar or closely linked destinations, meaning that riders would pay less
Our pledge is to donate 1% of annual profits to a local NGO or research institute working towards a sustainable environment, the beneficiary will be nominated by users.
The site allows users to get adequate information about the product, how the product works, Our pledge, read frequently asked questions, Sign Up to ride or Sign Up to be a driver, Contact Us, and why users should use ‘CarShare’.
Typical Users are: Working Middle Class, Students and the digitally savvy youth and groups of people with similar qualities and disabled people. Age group of typical users will be 16 - 45.